package org.designpatterns.mvc.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Model
 */
public class Store extends Observable {
    List<Item> itemList;

    public Store() {
        itemList = new ArrayList<>();
    }

    public void addItem(Item item) {
        itemList.add(item);
        notifyObservers();
    }

    public void removeItem(Item item) {
        itemList.remove(item);
        notifyObservers();
    }

    public List<Item> getItemList() {
        return this.itemList;
    }


}
