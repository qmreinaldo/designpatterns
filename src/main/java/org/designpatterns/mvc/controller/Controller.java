package org.designpatterns.mvc.controller;

import org.designpatterns.mvc.model.Item;
import org.designpatterns.mvc.model.Store;
import org.designpatterns.mvc.view.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {
    private Store store;
    private View view;

    public Controller(Store store, View view) {
        this.store = store;
        this.view = view;
        this.view.addItemButton.addActionListener(this);
        this.view.deleteItemButton.addActionListener(this);
    }

    public void beginView() {
        view.setContentPane(view.mainPanel);
        view.setTitle("Store");
        view.setLocationRelativeTo(null);
    }
    /**
     * Invoked when an action occurs.
     *
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String itemName = view.textFieldNameItem.getText();
        double itemPrice = Double.parseDouble(view.textFieldPriceItem.getText());
        Item item = new Item(itemName, itemPrice);
        store.addItem(item);
    }

}
