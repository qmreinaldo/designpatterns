package org.designpatterns.mvc;

import org.designpatterns.mvc.controller.Controller;
import org.designpatterns.mvc.model.Store;
import org.designpatterns.mvc.view.View;

public class Demo {
    public static void main(String[] args) {
        Store store = new Store();
        View view = new View();

        Controller controller = new Controller(store, view);

        controller.beginView();
        view.setVisible(true);
    }
}
