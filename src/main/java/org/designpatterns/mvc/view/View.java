package org.designpatterns.mvc.view;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame {
    public JTextField textFieldNameItem;
    public JTextField textFieldPriceItem;
    public JButton addItemButton;
    public JTextField textFieldNameProductToDelete;
    public JButton deleteItemButton;
    public JPanel mainPanel;

}
