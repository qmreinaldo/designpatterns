package org.designpatterns.structural.facade;

import java.math.BigDecimal;

public interface IAccount {
    void deposit(BigDecimal amount);
    void withdraw(BigDecimal amount);
    void transfer(IAccount receiverAccount, BigDecimal amount);
    int getAccountNumber();
}
