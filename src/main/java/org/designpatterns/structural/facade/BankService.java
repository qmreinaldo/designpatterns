package org.designpatterns.structural.facade;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BankService {
    private Map<Integer, IAccount> bankAccounts;

    public BankService() {
        this.bankAccounts = new HashMap<>();
    }

    public int createNewAccount(String typeAccount, BigDecimal initialAmount) {
        IAccount newAccount = null;

        switch (typeAccount) {
            case "chequing" -> newAccount = new ChequingAccount(initialAmount);
            case "saving" -> newAccount = new SavingAccount(initialAmount);
            case "investment" -> newAccount = new InvestmentAccount(initialAmount);
            default -> System.out.println("Invalid type account");
        }

        if (newAccount != null) {
            this.bankAccounts.put(newAccount.getAccountNumber(), newAccount);
            return newAccount.getAccountNumber();
        }

        return -1;
    }

    public void transferMoney(int to, int from, BigDecimal amount) {
        IAccount toAccount = this.bankAccounts.get(to);
        IAccount fromAccount = this.bankAccounts.get(from);
        fromAccount.transfer(toAccount, amount);
    }

    public IAccount getAccount(int number) {
        return bankAccounts.get(number);
    }
}
