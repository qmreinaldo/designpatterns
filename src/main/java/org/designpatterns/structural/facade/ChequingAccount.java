package org.designpatterns.structural.facade;

import java.math.BigDecimal;

public class ChequingAccount implements IAccount {
    private BigDecimal quantity;
    private final int accountNumber;

    private static int number = 100;

    public ChequingAccount(BigDecimal amount) {
        this.quantity = amount;
        accountNumber = number;
        number++;
    }
    @Override
    public void deposit(BigDecimal amount) {
        quantity = quantity.add(amount);
    }

    @Override
    public void withdraw(BigDecimal amount) {
        quantity = quantity.subtract(amount);
    }

    @Override
    public void transfer(IAccount receiverAccount, BigDecimal amount) {
        this.withdraw(amount);
        receiverAccount.deposit(amount);
    }

    @Override
    public int getAccountNumber() {
        return this.accountNumber;
    }

    @Override
    public String toString() {
        return "ChequingAccount{" +
                "quantity=" + quantity +
                '}';
    }
}
