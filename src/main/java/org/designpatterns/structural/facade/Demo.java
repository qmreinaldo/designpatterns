package org.designpatterns.structural.facade;

import java.math.BigDecimal;

public class Demo {
    public static void main(String[] args) {
        BankService myBankService = new BankService();
        int mySaving = myBankService.createNewAccount("saving", new BigDecimal("500.00"));
        myBankService.getAccount(mySaving).deposit(new BigDecimal("350.00"));
        System.out.println(myBankService.getAccount(mySaving));

        int myInvestment = myBankService.createNewAccount("investment", new BigDecimal("1000.00"));
        System.out.println(myBankService.getAccount(myInvestment));

        System.out.println("A transfer is made...");
        myBankService.transferMoney(myInvestment, mySaving, new BigDecimal("350.00"));

        System.out.println(myBankService.getAccount(mySaving));
        System.out.println(myBankService.getAccount(myInvestment));
    }
}
