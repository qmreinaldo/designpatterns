package org.designpatterns.structural.proxy;

import java.util.List;

/**
 * The proxy class
 */
public class OrderFulfillment implements IOrder {
    private List<WareHouse> warehouses;

    OrderFulfillment(List<WareHouse> warehouses) {
        this.warehouses = warehouses;
    }

    @Override
    public void fullFillOrder(Order order) {
        for (Item item : order.getItemList())
            for (WareHouse wareHouse : warehouses) {
                // We verify if the item is available before calling the real subject class
                if (wareHouse.currentInventory(item) != 0) {
                    wareHouse.fullFillOrder(order);
                    return;
                }
                else
                    System.out.println(item.getDescription() + " is not available in any warehouse");
                return;
            }
    }
}
