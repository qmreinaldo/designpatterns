package org.designpatterns.structural.proxy;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        Item milk = new Item("milk", 7.5, "001");
        Item yogurt = new Item("yogurt", 5.0, "002");

        Order order = new Order();
        order.addItem(milk);
        order.addItem(yogurt);

        WareHouse wareHouse1 = new WareHouse("Av. 123");
        wareHouse1.addItemToStock(milk, 2);
        wareHouse1.addItemToStock(yogurt, 1);

        WareHouse wareHouse2 = new WareHouse("Av. 987");
        wareHouse2.addItemToStock(milk, 1);

        List<WareHouse> wareHouseList = new ArrayList<>();
        wareHouseList.add(wareHouse1);
        wareHouseList.add(wareHouse2);

        OrderFulfillment orderFulfillment = new OrderFulfillment(wareHouseList);
        orderFulfillment.fullFillOrder(order);
        orderFulfillment.fullFillOrder(order);
        orderFulfillment.fullFillOrder(order);
    }
}
