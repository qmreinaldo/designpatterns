package org.designpatterns.structural.proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * Real subject class
 */
public class WareHouse implements IOrder {
    private Map<String, Integer> stock;
    private String address;

    WareHouse(String address) {
        stock = new HashMap<>();
        this.address = address;
    }

    public void addItemToStock(Item item, int nOfUnits) {
        if (!stock.containsKey(item.getCode()))
            stock.put(item.getCode(), nOfUnits);
        else {
            int currentNOfUnit = stock.get(item.getCode());
            stock.replace(item.getCode(), currentNOfUnit, currentNOfUnit + nOfUnits);
        }
    }

    @Override
    public void fullFillOrder(Order order) {
        for (Item item : order.getItemList()) {
            this.stock.replace(item.getCode(), stock.get(item.getCode())-1);
        }
        System.out.println("Ordered processed for shipment and delivery");
    }

    public int currentInventory(Item item) {
        if (stock.containsKey(item.getCode()))
            return stock.get(item.getCode());
        return 0;
    }
}
