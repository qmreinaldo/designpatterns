package org.designpatterns.structural.proxy;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private List<Item> itemList;
    public Order() {
        itemList = new ArrayList<>();
    }

    public void addItem(Item item) {
        itemList.add(item);
    }

    public List<Item> getItemList() {
        return this.itemList;
    }
}
