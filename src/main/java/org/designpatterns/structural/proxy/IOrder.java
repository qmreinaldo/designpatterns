package org.designpatterns.structural.proxy;

/**
 * Subject interface
 */
public interface IOrder {
    void fullFillOrder(Order order);
}
