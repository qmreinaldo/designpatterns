package org.designpatterns.structural.proxy;

public class Item {
    private final String description;
    private final double price;
    private final String code;

    public Item(String description, double price, String code) {
        this.description = description;
        this.price = price;
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }
}
