package org.designpatterns.structural.decorator;

/**
 * Component interface
 */
public interface WebPage {
    void display();
}
