package org.designpatterns.structural.decorator;

public class AuthenticatedWebPage extends WebPageDecorator {
    public AuthenticatedWebPage(WebPage webPage) {
        super(webPage);
    }

    public void authenticateUser() {
        System.out.println("Authenticating user ...");
    }

    @Override
    public void display() {
        super.display();
        this.authenticateUser();
    }
}
