package org.designpatterns.structural.decorator;

/**
 * Concrete component class
 */
public class BasicWebPage implements WebPage {
    private String html;
    private String css;
    private String js;
    @Override
    public void display() {
        System.out.println("Basic web page displayed");
    }
}
