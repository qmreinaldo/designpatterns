package org.designpatterns.structural.decorator;

public class AuthorizedWebPage extends WebPageDecorator {
    public AuthorizedWebPage(WebPage decoratorPage) {
        super(decoratorPage);
    }

    public void authorizeUser() {
        System.out.println("Authorizing user ...");
    }

    @Override
    public void display() {
        super.display();
        this.authorizeUser();
    }
}
