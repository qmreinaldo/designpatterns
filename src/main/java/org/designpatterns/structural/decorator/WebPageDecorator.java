package org.designpatterns.structural.decorator;

/**
 * Decorator
 */
public abstract class WebPageDecorator implements WebPage {
    protected WebPage webPage;  // aggregation
    public WebPageDecorator(WebPage webPage) {
        this.webPage = webPage;
    }

    @Override
    public void display() {
        this.webPage.display();
    }
}
