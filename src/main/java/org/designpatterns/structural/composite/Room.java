package org.designpatterns.structural.composite;

/**
 * The leaf class
 */
public class Room implements IStructure{
    private String name;

    public Room(String name) {
        this.name = name;
    }
    @Override
    public void enter() {
        System.out.println("You enter " + this.getName());
    }

    @Override
    public void exit() {
        System.out.println("You exit " + this.getName());
    }

    @Override
    public void location() {
        System.out.println("You are currently in " + this.getName());
    }

    @Override
    public String getName() {
        return this.name;
    }
}
