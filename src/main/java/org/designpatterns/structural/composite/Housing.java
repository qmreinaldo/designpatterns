package org.designpatterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

/**
 * The composite class
 */

public class Housing implements IStructure {
    private final List<IStructure> structures;
    private final String address;

    public Housing(String address) {
        this.structures = new ArrayList<>();
        this.address = address;
    }

    @Override
    public String getName() {
        return this.address;
    }

    /**
     * Add a new a component to the structure.
     * @param component it could be a composite or a leaf
     * @return the index for the new component in the list of structures
     */
    public int addStructure(IStructure component) {
        this.structures.add(component);
        return structures.size()-1;
    }


    public IStructure getStructure(int componentIndex) {
        return this.structures.get(componentIndex);
    }

    @Override
    public void location() {
        System.out.println("You are currently in " + this.getName() + ".\n " +
                "It has " );
        for (IStructure structure : this.structures)
            System.out.println(structure.getName());
    }

    @Override
    public void enter() {
        System.out.println("You enter into " + this.getName());
    }

    @Override
    public void exit() {
        System.out.println("You exit from " + this.getName());
    }

}
