package org.designpatterns.structural.composite;

public class Demo {
    public static void main(String[] args) {
        Housing building = new Housing("221 Baker Street");
        Housing floor1 = new Housing("221 Baker Street - first floor");

        int firstFloor = building.addStructure(floor1);

        Room washRoomMen = new Room("Men's washroom");
        Room washRoomWomen = new Room("Women's washroom");
        Room livingRoom = new Room("living room");

        int firstFloorMen = floor1.addStructure(washRoomMen);
        int firstFloorWomen = floor1.addStructure(washRoomWomen);
        int firstLivingRoom = floor1.addStructure(livingRoom);

        building.enter();
        Housing currentFloor = (Housing) building.getStructure(firstFloor);

        currentFloor.enter();   // Walk into the first floor

        Room currentRoom = (Room) currentFloor.getStructure(firstFloorMen);
        currentRoom.enter();    // Walk into the second floor

        currentRoom = (Room) currentFloor.getStructure(firstLivingRoom);
        currentRoom.enter();    // Walk into living room

        building.exit();

    }
}
