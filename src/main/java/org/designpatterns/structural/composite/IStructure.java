package org.designpatterns.structural.composite;

/**
 * The interface that defines the overall type
 */
public interface IStructure {
    void enter();
    void exit();
    void location();
    String getName();
}
