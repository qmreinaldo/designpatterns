package org.designpatterns.structural.adapter;

public class Client {
    public static void main(String[] args) {
        String webHost = "https://myweb/";
        WebService service = new WebService(webHost);
        WebAdapter adapter = new WebAdapter();
        adapter.connect(service);
        WebClient webClient = new WebClient(adapter);
        webClient.doWork();
    }
}
