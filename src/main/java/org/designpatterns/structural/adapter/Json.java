package org.designpatterns.structural.adapter;

public class Json {
    private String body;
    Json(String body) {
        this.body = body;
    }

    public String getBody() {
        return this.body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
