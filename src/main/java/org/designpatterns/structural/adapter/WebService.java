package org.designpatterns.structural.adapter;

/**
 * The adapted class
 */
public class WebService {
    private final String webHost;
    WebService(String webHost) {
        this.webHost = webHost;
    }
    public Json request(Json request) {
        return new Json(request.getBody() + " was processed by the external library");
    }
}
