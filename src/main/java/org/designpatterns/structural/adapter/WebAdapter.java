package org.designpatterns.structural.adapter;

/**
 * The adapter class
 */
public class WebAdapter implements WebRequester {
    private WebService service;
    public void  connect(WebService currentService) {
        this.service = currentService;
    }
    @Override
    public int request(Object request) {
        Json result = this.toJson(request);
        Json response = service.request(result);
        if (response != null)
            return 200;     // OK status code
        return 500;         // Server error code
    }
    private Json toJson(Object request) {
        return new Json((String) request);
    }
}
