package org.designpatterns.structural.adapter;

/**
 * The client class
 */
public class WebClient {
    private final WebRequester webRequester;

    public WebClient(WebRequester webRequester) {
        this.webRequester = webRequester;
    }

    private Object makeObject() {
        String obj = "Data";
        return obj;
    }

    public void doWork() {
        Object obj = makeObject();
        int status = webRequester.request(obj);
        if (status == 200)
            System.out.println("OK");
        else
            System.out.println("Not OK");
    }
}
