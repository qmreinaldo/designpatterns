package org.designpatterns.structural.adapter;

/**
 * The target interface
 */
public interface WebRequester {
    int request(Object request);
}
