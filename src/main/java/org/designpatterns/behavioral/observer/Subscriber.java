package org.designpatterns.behavioral.observer;


public class Subscriber implements Observer {
    private final String name;
    public Subscriber(String name) {
        this.name = name;
    }

    @Override
    public void update(String status) {
        System.out.println(name + " is notified, " +
                "blog status change to " + status );
    }
}
