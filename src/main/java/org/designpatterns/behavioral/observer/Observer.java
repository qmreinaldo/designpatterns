package org.designpatterns.behavioral.observer;

public interface Observer {
    void update(String status);
}
