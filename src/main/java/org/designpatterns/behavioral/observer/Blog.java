package org.designpatterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class Blog implements Subject {
    private final List<Observer> observers = new ArrayList<>();
    private String name;
    private String status;
    Blog(String name, String status) {
        this.name = name;
        this.status = status;
    }


    @Override
    public void registerObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void unregisterObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObserver(String status) {
        for (Observer observer : observers)
            observer.update(status);
    }

    public void setStatus(String status) {
        if (!this.status.equals(status))
            notifyObserver(status);
        this.status = status;
    }

}
