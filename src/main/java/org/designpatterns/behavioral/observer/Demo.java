package org.designpatterns.behavioral.observer;

public class Demo {
    public static void main(String[] args) {
        Blog blog = new Blog("The Turing machine Blog", "active");

        Observer subscriber1 = new Subscriber("Alice");
        Observer subscriber2 = new Subscriber("Bob");

        blog.registerObserver(subscriber1);
        blog.registerObserver(subscriber2);

        blog.setStatus("inactive");
    }
}
