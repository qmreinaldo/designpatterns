package org.designpatterns.behavioral.command;

public class Demo {
    public static void main(String[] args) {
        CommandManager commandManager = CommandManager.getInstance();
        Command command;

        Document document = new Document();

        command = new PasteCommand(document, 0, "Hello world!");
        commandManager.executeCommand(command);

        System.out.println(document);
    }
}
