package org.designpatterns.behavioral.command;

import java.util.ArrayList;
import java.util.List;

/**
 * The receiver class
 */

public class Document {
    private final List<Character> characterList;
    public Document() {
        characterList = new ArrayList<>();
    }

    public Document(String initialText) {
        this();
        for (char ch : initialText.toCharArray())
            characterList.add(ch);
    }

    public void insertText(int index, String text) {
        for (int i = 0; i < text.length(); i++)
            characterList.add(index+i, text.charAt(i));
    }

    public void deleteText(int index, int length) {
        for (int i = 0; i < length; i++)
            characterList.remove(index);
    }

    @Override
    public String toString() {
        StringBuilder text = new StringBuilder();
        for (char ch : characterList)
            text.append(ch);
        return text.toString();
    }
}
