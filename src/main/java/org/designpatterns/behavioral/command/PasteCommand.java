package org.designpatterns.behavioral.command;

public class PasteCommand implements Command {
    private Document document;
    private int index;
    private String text;

    PasteCommand (Document document, int index, String text) {
        this.document = document;
        this.index = index;
        this.text = text;
    }

    @Override
    public void execute() {
        document.insertText(this.index, this.text);
    }

    @Override
    public void undo() {
        document.deleteText(this.index, this.text.length());
    }

    @Override
    public boolean isReversible() {
        return true;
    }
}
