package org.designpatterns.behavioral.command;

public class CommandManager {
    public void executeCommand(Command command) {
        command.execute();
    }

    public static CommandManager getInstance() {
        return new CommandManager();
    }
}
