package org.designpatterns.behavioral.command;

/*
The command class
 */
public interface Command {
    void execute();
    void undo();
    boolean isReversible();
}
