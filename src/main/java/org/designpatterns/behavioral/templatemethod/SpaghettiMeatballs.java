package org.designpatterns.behavioral.templatemethod;

public class SpaghettiMeatballs extends PastaDish {

    @Override
    protected void addPaste() {
        System.out.println("Adding spaghetti ....");
    }

    @Override
    protected void addSauce() {
        System.out.println("Adding tomato sauce ...");
    }

    @Override
    protected void addProtein() {
        System.out.println("Adding meatballs ...");
    }

    @Override
    protected void addGarnish() {
        System.out.println("Adding cheese ...");
    }
}
