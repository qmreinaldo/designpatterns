package org.designpatterns.behavioral.templatemethod;

/**
 * The super class is abstract
 */
public abstract class PastaDish {
    /**
     * The common method is set to be final to prevent
     * be overriding in the subclasses.
     */
    public final void makeRecipe() {
        boilWater();
        cookPasta();
        drainAndPlate();
        addPaste();
        addSauce();
        addProtein();
        addGarnish();
    }

    private void boilWater() {
        System.out.println("Boiling water ...");
    }

    private void cookPasta() {
        System.out.println("Cooking pasta ...");
    }

    private void drainAndPlate() {
        System.out.println("Draining and plating ...");
    }
    protected abstract void addPaste();
    protected abstract void addSauce();
    protected abstract void addProtein();
    protected abstract void addGarnish();

}
