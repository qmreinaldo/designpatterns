package org.designpatterns.behavioral.templatemethod;

public class Demo {
    public static void main(String[] args) {
        PastaDish pastaDish;

        pastaDish = new PenneAlfredo();
        pastaDish.makeRecipe();

        pastaDish = new SpaghettiMeatballs();
        pastaDish.makeRecipe();

    }
}
