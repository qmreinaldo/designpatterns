package org.designpatterns.behavioral.state;

public class OutOfState implements State {
    @Override
    public void insertDollar(ChocolateMachine chocolateMachine) {
        System.out.println("Sorry! No more chocolates");
    }

    @Override
    public void eject(ChocolateMachine chocolateMachine) {
        System.out.println("Sorry! No more chocolates");
    }

    @Override
    public void dispense(ChocolateMachine chocolateMachine) {
        System.out.println("Sorry! No more chocolates");
    }
}
