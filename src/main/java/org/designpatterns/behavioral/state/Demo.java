package org.designpatterns.behavioral.state;

import org.designpatterns.behavioral.state.ChocolateMachine;

public class Demo {
    public static void main(String[] args) {
        ChocolateMachine chocolateMachine = new ChocolateMachine(3);

        chocolateMachine.insertDollar();
        chocolateMachine.dispense();

        chocolateMachine.insertDollar();
        chocolateMachine.dispense();

        chocolateMachine.insertDollar();
        chocolateMachine.ejectMoney();

        chocolateMachine.insertDollar();
        chocolateMachine.dispense();

        chocolateMachine.insertDollar();


    }

}
