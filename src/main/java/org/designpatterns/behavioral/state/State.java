package org.designpatterns.behavioral.state;

public interface State {
    void insertDollar(ChocolateMachine chocolateMachine);
    void eject(ChocolateMachine chocolateMachine);
    void dispense(ChocolateMachine chocolateMachine);
}
