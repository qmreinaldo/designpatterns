package org.designpatterns.behavioral.state;

public class IdleState implements State {
    @Override
    public void insertDollar(ChocolateMachine chocolateMachine) {
        System.out.println("Dollar inserted");
        chocolateMachine.setCurrentState(chocolateMachine.getHasOneDollarState());
    }

    @Override
    public void eject(ChocolateMachine chocolateMachine) {
        System.out.println("No money to return");
    }

    @Override
    public void dispense(ChocolateMachine chocolateMachine) {
        System.out.println("Insert one dollar");
    }
}
