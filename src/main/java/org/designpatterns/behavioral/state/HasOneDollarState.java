package org.designpatterns.behavioral.state;

public class HasOneDollarState implements State {
    @Override
    public void insertDollar(ChocolateMachine chocolateMachine) {
        System.out.println("Dollar already inserted");
    }

    @Override
    public void eject(ChocolateMachine chocolateMachine) {
        System.out.println("Returning money ....");

        chocolateMachine.returnMoney();
        chocolateMachine.setCurrentState(chocolateMachine.getIdleState());
    }

    @Override
    public void dispense(ChocolateMachine chocolateMachine) {
        System.out.println("Dispensing...");

        if (chocolateMachine.getNumOfChocolates() > 1) {
            chocolateMachine.giveTheChocolate();
            chocolateMachine.setCurrentState(chocolateMachine.getIdleState());
        }
        else {
            chocolateMachine.giveTheChocolate();
            chocolateMachine.setCurrentState(chocolateMachine.getOutOfState());
        }
        chocolateMachine.decrementNumberOfChocolates();
    }
}
