package org.designpatterns.behavioral.state;

public class ChocolateMachine {
    private State idleState;
    private State hasOneDollarState;
    private State outOfState;
    private State currentState;
    private int numOfChocolates;

    public ChocolateMachine(int numOfChocolates) {
        idleState = new IdleState();
        hasOneDollarState = new HasOneDollarState();
        outOfState = new OutOfState();

        if (numOfChocolates > 0) {
            currentState = idleState;
            this.numOfChocolates = numOfChocolates;
        } else {
            currentState = outOfState;
            this.numOfChocolates = 0;
        }
    }

    public void setCurrentState (State currentState) {
        this.currentState = currentState;
    }

    public int getNumOfChocolates () {
        return this.numOfChocolates;
    }

    public State getIdleState() {
        return this.idleState;
    }

    public State getOutOfState() {
        return this.outOfState;
    }

    public State getHasOneDollarState() {
        return this.hasOneDollarState;
    }

    public void insertDollar() {
        currentState.insertDollar(this);
    }

    public void ejectMoney() {
        currentState.eject(this);
    }

    public void dispense() {
        currentState.dispense(this);
    }

    public void returnMoney() {
        System.out.println("Take your money back");
    }

    public void giveTheChocolate() {
        System.out.println("Take it your chocolate");
    }

    public void decrementNumberOfChocolates() {
        this.numOfChocolates--;
    }
}
