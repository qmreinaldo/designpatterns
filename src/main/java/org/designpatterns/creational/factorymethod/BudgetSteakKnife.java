package org.designpatterns.creational.factorymethod;

/**
 * A Concrete Product
 */
public class BudgetSteakKnife extends Knife {
    @Override
    public void sharpen() {
        System.out.println("Budget steak knife is sharpened");
    }

    @Override
    public void polish() {
        System.out.println("Budget steak knife is polished");
    }

    @Override
    public void pack() {
        System.out.println("Budget steak knife is packed");
    }
}
