package org.designpatterns.creational.factorymethod;

/**
 * The Creator class
 */
public abstract class KnifeStore {
    public Knife orderKnife(String knifeType) {
        Knife knife;
        knife = createKnife(knifeType);

        knife.sharpen();
        knife.polish();
        knife.pack();

        return knife;
    }

    /**
     * The factory method that must be implemented by the Creator subclasses
     * @param knifeType
     * @return
     */
    abstract Knife createKnife(String knifeType);
}
