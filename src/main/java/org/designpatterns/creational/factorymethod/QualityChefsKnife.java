package org.designpatterns.creational.factorymethod;

/**
 * A Concrete Product
 */
public class QualityChefsKnife extends Knife {
    @Override
    public void sharpen() {
        System.out.println("Quality chefs knife is sharpened");
    }

    @Override
    public void polish() {
        System.out.println("Quality chefs knife is polished");
    }

    @Override
    public void pack() {
        System.out.println("Quality chefs knife is packed");
    }
}
