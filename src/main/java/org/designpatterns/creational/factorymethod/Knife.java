package org.designpatterns.creational.factorymethod;

/**
 * The Product class
 */
public abstract class Knife {
    public abstract void sharpen();
    public abstract void polish();
    public abstract void pack();
}
