package org.designpatterns.creational.factorymethod;

/**
 * A Concrete Product
 */
public class QualitySteakKnife extends Knife {
    @Override
    public void sharpen() {
        System.out.println("Quality steak knife is sharpened");
    }

    @Override
    public void polish() {
        System.out.println("Quality steak knife is polished");
    }

    @Override
    public void pack() {
        System.out.println("Quality steak knife is packed");
    }
}
