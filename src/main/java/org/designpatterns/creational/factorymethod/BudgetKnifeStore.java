package org.designpatterns.creational.factorymethod;

/**
 * A Concrete Creator
 */

public class BudgetKnifeStore extends KnifeStore{

    /**
     * A concrete factory method
     * @param knifeType
     * @return
     */
    @Override
    Knife createKnife(String knifeType) {
        Knife knife = null;
        if (knifeType.equals("steak"))
            knife = new BudgetSteakKnife();
        else if (knifeType.equals("chefs"))
            knife = new BudgetChefsKnife();
        return knife;
    }
}
