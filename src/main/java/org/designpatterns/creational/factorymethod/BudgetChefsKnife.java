package org.designpatterns.creational.factorymethod;

/**
 * A Concrete Product
 */
public class BudgetChefsKnife extends Knife {

    @Override
    public void sharpen() {
        System.out.println("Budget chefs knife is sharpened");
    }

    @Override
    public void polish() {
        System.out.println("Budget chefs knife is polished");
    }

    @Override
    public void pack() {
        System.out.println("Budget chefs knife is packed");
    }
}
