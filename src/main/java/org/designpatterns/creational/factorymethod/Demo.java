package org.designpatterns.creational.factorymethod;

public class Demo {
    public static void main(String[] args) {
        KnifeStore knifeStore;

        knifeStore = new BudgetKnifeStore();
        knifeStore.orderKnife("steak");
        knifeStore.orderKnife("chefs");

        knifeStore = new QualityKnifeStore();
        knifeStore.orderKnife("steak");
        knifeStore.orderKnife("chefs");
    }
}
