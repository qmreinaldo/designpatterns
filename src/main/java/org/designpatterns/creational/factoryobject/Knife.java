package org.designpatterns.creational.factoryobject;

public abstract  class Knife {
    public abstract void sharpe();

    public abstract void polish();

    public abstract void pack();
}
