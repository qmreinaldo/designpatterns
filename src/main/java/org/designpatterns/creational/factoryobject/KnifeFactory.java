package org.designpatterns.creational.factoryobject;

public class KnifeFactory {
    /**
     * Method for concrete instantiation
     * @param knifeType
     * @return a concrete knife
     */
    public Knife createKnife(String knifeType) {
        Knife knife = null;
        if (knifeType.equals("steak"))
            knife = new SteakKnife();
        else if (knifeType.equals("chefs"))
            knife = new ChefsKnife();
        return knife;
    }
}
