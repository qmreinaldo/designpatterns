package org.designpatterns.creational.factoryobject;

public class KnifeStore {
    private KnifeFactory factory;

    /**
     * Constructor needs a KnifeFactory
     * @param factory
     */
    public KnifeStore(KnifeFactory factory) {
        this.factory = factory;
    }

    public Knife orderKnife(String knifeType) {
        Knife knife;
        // Call the create method of the factory
        knife = factory.createKnife(knifeType);

        // Prepare the knife
        knife.sharpe();
        knife.polish();
        knife.pack();

        return knife;
    }
}
