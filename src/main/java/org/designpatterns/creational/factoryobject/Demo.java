package org.designpatterns.creational.factoryobject;

public class Demo {
    public static void main(String[] args) {
        KnifeFactory knifeFactory = new KnifeFactory();
        KnifeStore knifeStore = new KnifeStore(knifeFactory);

        knifeStore.orderKnife("chefs");
        knifeStore.orderKnife("steak");
    }
}
