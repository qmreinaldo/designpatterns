package org.designpatterns.creational.factoryobject;

public class SteakKnife extends Knife {
    @Override
    public void sharpe() {
        System.out.println("SteakKnife is sharpened");
    }

    @Override
    public void polish() {
        System.out.println("SteakKnife is polished");
    }

    @Override
    public void pack() {
        System.out.println("SteakKnife is packed");
    }
}
