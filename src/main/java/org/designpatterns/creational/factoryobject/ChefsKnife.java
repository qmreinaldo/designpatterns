package org.designpatterns.creational.factoryobject;

public class ChefsKnife extends Knife {
    @Override
    public void sharpe() {
        System.out.println("ChefsKnife is sharpened.");
    }

    @Override
    public void polish() {
        System.out.println("ChefsKnife is polished.");
    }

    @Override
    public void pack() {
        System.out.println("ChefsKnife is packed.");
    }
}
