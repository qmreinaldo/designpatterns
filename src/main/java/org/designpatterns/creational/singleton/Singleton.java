package org.designpatterns.creational.singleton;

public class Singleton {
    /*
    This instance variable is made static because
    it needs to be shared among all instance of the class.
     */

    private static Singleton uniqueInstance = null;

    /*
    Constructor must be private to prevent multiple
    instantiation of Singleton class.
     */
    private Singleton() {
        // Initialization code
    }

    /*
    The method is static because it's intended to provide access
    to the unique instance without the need to create an object
    of this class.
     */
    public static Singleton getInstance() {
        if (uniqueInstance == null)
            uniqueInstance = new Singleton();
        return uniqueInstance;
    }
}


