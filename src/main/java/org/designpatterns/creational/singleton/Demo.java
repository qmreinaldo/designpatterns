package org.designpatterns.creational.singleton;

public class Demo {
    public static void main(String[] args) {
        Singleton singleton1 = Singleton.getInstance(); // Use the static method
        Singleton singleton2 = Singleton.getInstance(); // Use the static method

        System.out.println("singleton1 and singleton2 reference to the same instance? " +
                (singleton1 == singleton2 ? "Yes" : "No"));
    }
}
